#!/bin/sh

docker-compose run --rm wait_for_infra
docker-compose up --no-deps -d logstash
docker-compose up --no-deps -d filebeat
echo "Started"
