# Elastic with filebeat stack

Easy log parsing with easy viewing in kibana.

No data persistence.

## Description

This docker-compose.yml will setup 4 containers:
- elasticsearch
- kibana
- logstash
- filebeat

Assuming you are in the docker group, just run:

    ./quickstart.sh

## Prerequisites

- docker
- docker-compose (handling compose file version 3)
